# Projet wonder 

Ce projet va nous permettre de mettre en pratique les notions de base de symfony.

## Installation

Utiliser la commande suivante pour installer le projet :

`symfony new wonderApp --webapp`

## Utilisation

Pour utiliser le projet, il suffit de lancer le serveur avec la commande suivante :
`cd wonderApp`

`symfony server:start`


## Les dépendances

- Le profiler de symfony : permet de voir les requêtes, les variables, les erreurs, etc lors du développement.
  
  `symfony composer req profiler --dev` 


## Les controllers & formulaires

- Créer un controller : `symfony console make:controller` 

1. Création d'un controller pour les questions : 
  `symfony console make:controller QuestionController`

2. Création du formulaire pour les questions : 
  `symfony console make:form QuestionType`

3. Création de la base de données : 
  `symfony console doctrine:database:create` nommée `wonder`

4. Création de la table `question` :
  `symfony console make:entity` nommée `Question` avec les champs suivants :
  - `id` de type `integer` et `auto-increment`
  - `title` de type `string` et `length` de `255`
  - `content` de type `text`
  - `rating` de type `integer` et `default` de `0`
  - `createdAt` de type `datetime_immutable` et `default` de `now`
  - `nbrOfResponse` de type `integer` et `default` de `0`

5. Création des migrations : 
  `symfony console make:migration`
  `symfony console doctrine:migrations:migrate`

6. Création de l'entité `Comment` :
  `symfony console make:entity` nommée `Comment` avec les champs suivants :
  - `id` de type `integer` et `auto-increment`
  - `content` de type `text`
  - `rating` de type `integer` et `default` de `0`
  - `createdAt` de type `datetime_immutable` et `default` de `now`
  - `question` de type `relation` et `ManyToOne` avec l'entité `Question`

7. Création des migrations : 
  `symfony console make:migration`
  `symfony console doctrine:migrations:migrate`

8. Création du formulaire pour les commentaires : 
  `symfony console make:form CommentType`

9. Ajout de `knpTimeBundle` pour afficher les dates en français :
  `composer require knplabs/knp-time-bundle`

10. Ajout du composant security: 
   `symfony composer req security` 

11. Ajout du bundle `rate-limiter` pour limiter le nombre de requêtes afin d'éviter les attaques par force brute :
   `symfony composer req rate-limiter`

12. Création de l'entité `User`
    `symfony console make:user` nommée `User` avec les champs suivants :
     - `id` de type `integer` et `auto-increment`
     - `email` de type `string` et `length` de `180`

13. Ajout des autres champs à l'entité `User` :
    `symfony console make:entity` nommée `User` avec les champs suivants :
     - `id` de type `integer` et `auto-increment`
     - `gender` de type `string` et `length` de `10`
     - `firstName` de type `string` et `length` de `255`
     - `lastName` de type `string` et `length` de `255`
     - `questions` de type `relation` et `OneToMany` avec l'entité `Question`
     - `comments` de type `relation` et `OneToMany` avec l'entité `Comment`
     - `createdAt` de type `datetime_immutable` et `default` de `now`
     - `picture` de type `string` et `length` de `255`

14. Création du formulaire pour l'inscription : 
    `symfony console make:form` basé sur l'entité `User`

15. Création du controller `SecurityController` :
    `symfony console make:controller SecurityController`

16. Création du controller  `UserController` :
    `symfony console make:controller UserController`

17. Création de l'entité `Vote` pour gére les votes.
   `Un utilisateur peut voter pour une question ou un réponse. Un utilisateur ne peut voter qu'une seule fois pour une question ou un réponse.Un vote peut être positif ou négatif.`

    `symfony console make:entity` nommée `Vote` avec les champs suivants :
     - `author` de type `relation` et `ManyToOne` avec l'entité `User`
     - `question` de type `relation` et `ManyToOne` avec l'entité `Question`
     - `comment` de type `relation` et `ManyToOne` avec l'entité `Comment`
     - `isLiked` de type `boolean` et `default` de `false`


18. Ajout du coomposant mailer
    `symfony composer req mailer`

19. Création d'un compte sur mailtrap.io pour tester l'envoi de mail
    `https://mailtrap.io/`

20. Ajout des extensions twig pour faire du css inline
    `symfony composer req twig/cssinliner-extra twig/extra-bundle` ici on a besoin de deux extensions pour faire du css inline
  - twig/extra-bundle : Permet d'utiliser les extensions twig
  - twig/cssinliner-extra : Permet de faire du css inline


21. Création de l'entité `ResetPassword` pour gérer les demandes de réinitialisation de mot de passe
    `symfony console make:entity` nommée `ResetPassword` avec les champs suivants :
     - `user` de type `relation` et `OneToOne` avec l'entité `User`
     - `token` de type `string` et `length` de `255`
     - `expiresAt` de type `datetime_immutable` et `default` de `now`
     - `createdAt` de type `datetime_immutable` et `default` de `now`

22. Ajout de `webpack` encore pour gérer les assets
    `symfony composer req encore`


## MISE EN PROD SUR VPS

- Création d'un compte sur IONOS et achat d'un VPS

- Connexion au VPS en SSH
  `ssh root@MON IP`

- Mettre à jour le système
  `apt update && apt upgrade`

- Redémarer le serves
  `sudo reboot`

- Installation de mysql
  `sudo apt install mysql-server`

- Vérification du status du service mysql
  `sudo systemctl status mysql` OU `sudo service mysql status`

- Ajouter de l'utilisteur root
  `sudo mysql_secure_installation`

- Connexion à mysql
  `sudo mysql -u root`

- Créatiion d'un utilisateur
  `CREATE USER 'wonder'@'localhost' IDENTIFIED BY 'azertyUIOP123@';`

- Ajout des permissions à l'utilisateur
  `GRANT ALL PRIVILEGES ON *.* TO 'wonder'@'localhost' WITH GRANT OPTION;`

- Voir la liste des utilisateurs
  `SELECT User FROM mysql.user;`

- Connexion à mysql avec l'utilisateur `wonder`
  `mysql -u wonder -p azertyUIOP123@`

- Voir l'utilisateur courant
  `SELECT CURRENT_USER();`

- Création d'un autre utilisateur qui aura les acces en dehors de localhost
  `CREATE USER 'liam'@'%' IDENTIFIED BY 'AZERTYUIOP123';`

- Attribution des permissions à l'utilisateur
  `GRANT ALL PRIVILEGES ON *.* TO 'liam'@'%' WITH GRANT OPTION;`

- Valider les changements
  `FLUSH PRIVILEGES;`

- Supprimer un utilisateur
  `DROP USER 'liam'@'%';`

- Définir le mot de passe de l'utilisateur `root`
  `sudo passwd root`

- Redemarer le serveur
  `sudo reboot` `sudo service sshd restart`
- Activer l'autocomplétion php
  `LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php` Cette commande permet d'ajouter le dépôt `ppa:ondrej/php` qui contient les versions les plus récentes de php

- Installation de php
  `apt install php8.1-fpm`

- Installation de composer
  `php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"`

- Déplacer composer dans le dossier bin
  `mv composer.phar /bin/composer`

- Installer symfony
  `wget https://get.symfony.com/cli/installer -O - | bash`

- Déplacer symfony dans le dossier bin
  ` mv /root/.symfony5/bin/symfony /usr/local/bin/symfony`

- Vérifier les requirements
  `symfony check:requirements`

- Ajout des extensions php
  `apt install php8.1-mysql php8.1-xml php8.1-mbstring php8.1-curl php8.1-intl php8.1-gd php8.1-zip php8.1-bcmath`
  `apt install php8.1-zip php8.1-unzip`

  - Se déplacer dans le dossier home
  `cd /home`

- clone du projet
  `git clone https......`

- Se déplacer dans le dossier du projet
  `cd wonderApp`

- Installation des dépendances
  `symfony composer install --no-dev --optimize-autoloader`

- Création du fichier .env.local.php
  `composer dump-env prod`
- Ajout de nvm pour installer node
  `wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash`

  `nvm install --lts` Pour installer la dernière version de node

- Build des assets
  `npm install`
  `npm run build`

- Installation de nginx
  `apt install nginx`
- vérification du status de nginx
  `systemctl status nginx`




## Notes

`app.request.pathinfo` contient le chemin de la page actuelle

`app.request.uri` contient l'uri de la page actuelle c'est à dire le chemin de la page actuelle avec les paramètres

# EVENT SQL

Un event SQL est un objet qui permet d'exécuter une requête SQL à un moment donné.

## Création d'un event SQL pour nétoyer la table `reset_password` chaque jour à minuit

`CREATE EVENT ClearExpiredTokens ON SCHEDULE EVERY 5 MINUTE COMMENT 'Nettoie la table reset_password toutes les 5 minutes' DO DELETE FROM wonder.reset_password WHERE expired_at < NOW();`

## Vérification de la création de l'event SQL

`SHOW EVENTS;` OU `SHOW EVENTS FROM wonder;`

## Vérification si l'event SQL est actif

`SHOW VARIABLES LIKE 'event_scheduler';`

## Activation de l'event SQL

`SET GLOBAL event_scheduler = ON;`

## Vérifier si le service est de planification des événements est actif

`SHOW PROCESSLIST;`

## Suppression de l'event SQL

`DROP EVENT ClearExpiredTokens;`


