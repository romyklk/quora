document.addEventListener('DOMContentLoaded', function () {
    // Sélectionnez la div avec l'id flash
    var flashDiv = document.getElementById('flash');

    // Vérifiez si la div existe avant de définir le délai
    if (flashDiv) {
        // Masquez la div après 10 secondes
        setTimeout(function () {
            flashDiv.style.display = 'none';
        }, 5000);
    }
}); 