<?php

namespace App\Controller;

use App\Repository\QuestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(QuestionRepository $questionRepository): Response
    {

        /* $data = $questionRepository->findBy(
            [],        // Aucun critère de filtre car on veut toutes les questions
            ['createdAt' => 'DESC'] 
        ); */
        $data = $questionRepository->findQuestionsWithAuthor();

        $questions = [];

        foreach ($data as $question) {
            $questions[] = [
                'id' => $question->getId(),
                'title' => $question->getTitle(),
                'content' => $question->substringsWithoutBreakingWords($question->getContent(), 400),
                'createdAt' => $question->getCreatedAt(),
                'rating' => $question->getRating(),
                'nbrOfResponse' => $question->getNbrOfResponse(),
                'author' => $question->getAuthor(),
            ];
        }
        return $this->render('home/index.html.twig', [
            'questions' => $questions,
        ]);
    }
}
