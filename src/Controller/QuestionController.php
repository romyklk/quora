<?php

namespace App\Controller;

use App\Entity\Vote;
use App\Entity\Comment;
use App\Entity\Question;
use App\Form\CommentType;
use App\Form\QuestionType;
use App\Repository\VoteRepository;
use App\Repository\CommentRepository;
use App\Repository\QuestionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class QuestionController extends AbstractController
{
    #IS_AUTHENTICATED_FYLLY : l'utilisateur doit être authentifié totalement pour accéder à la page
    #IS_AUTHENTICATED_REMEMBERED : l'utilisateur doit être authentifié totalement ou partiellement pour accéder à la page. Partiellement signifie que l'utilisateur peut être authentifié via un cookie
    #IS_AUTHENTICATED_ANONYMOUSLY : l'utilisateur peut accéder à la page sans être authentifié

    #[Route('/question/ask', name: 'app_question_ask')]
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function index(Request $request, EntityManagerInterface $em): Response
    {
        $user = $this->getUser();

        $question = new Question();

        $formQuestion = $this->createForm(QuestionType::class, $question);

        $formQuestion->handleRequest($request);

        if ($formQuestion->isSubmitted() && $formQuestion->isValid()) {

            $question->setRating(0);
            $question->setNbrOfResponse(0);
            $question->setCreatedAt(new \DateTimeImmutable());
            $question->setAuthor($user);
            $em->persist($question);
            $em->flush();

            //$this->addFlash('success', 'Votre question a bien été ajoutée');

            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addSuccess('Votre question a bien été ajoutée.');

            return $this->redirectToRoute('app_question_show', [
                'id' => $question->getId()
            ]);
        }

        return $this->render('question/index.html.twig', [
            'form' => $formQuestion->createView(),
        ]);
    }

    #[Route('/question/{id}', name: 'app_question_show')]
    public function show(Request $request, int $id, QuestionRepository $questionRepository, EntityManagerInterface $em): Response
    {
        $user = $this->getUser();
        //$question = $questionRepository->find($id);
        $question = $questionRepository->findQuestionWithCommentsAndAuthor($id);

        $options = [
            'question' => $question
        ];

        if ($user) {

            if (!$question) {
                throw $this->createNotFoundException("La question demandée n'existe pas");
            }

            $comment = new Comment();

            $commentForm = $this->createForm(CommentType::class, $comment);

            $commentForm->handleRequest($request);

            if ($commentForm->isSubmitted() && $commentForm->isValid()) {

                $comment->setCreatedAt(new \DateTimeImmutable());
                $comment->setQuestion($question);
                $comment->setRating(0);
                $question->setNbrOfResponse($question->getNbrOfResponse() + 1);
                $comment->setAuthor($user);
                $em->persist($comment);
                $em->flush();

                //$this->addFlash('success', 'Votre réponse a bien été ajouté');

                notyf()
                    ->position('x', 'center')
                    ->position('y', 'top')
                    ->duration(5000)
                    ->dismissible(true)
                    ->addSuccess('Votre réponse a bien été ajouté.');

                return $this->redirectToRoute('app_question_show', [
                    'id' => $question->getId()
                ]);
            }
            $options['form'] = $commentForm->createView();
        }

        return $this->render('question/show.html.twig', $options);
    }

    #[Route('/question/rating/{id}/{score}', name: 'question_rating')]
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function ratingQuestion(Request $request, Question $question, VoteRepository $voteRepository, int $score, EntityManagerInterface $em)
    {
        $user = $this->getUser();

        if ($user !== $question->getAuthor()) {
            $vote = $voteRepository->findOneBy(['author' => $user, 'question' => $question]);

            if ($vote) {

                if (($vote->getIsLiked() && $score > 0) || (!$vote->getIsLiked() && $score < 0)) {
                    $em->remove($vote);
                    $question->setRating($question->getRating() + ($score > 0 ? -1 : 1));
                } else {
                    $vote->setIsLiked(!$vote->getIsLiked());
                    $question->setRating($question->getRating() + ($score > 0 ? 2 : -2));
                }
            } else {
                $vote = new Vote();
                $vote->setAuthor($user);
                $vote->setQuestion($question);
                $vote->setIsLiked($score > 0 ? true : false);
                $question->setRating($question->getRating() + $score);
                $em->persist($vote);
            }

            $em->flush();
        }



        $referer = $request->server->get('HTTP_REFERER');
        return $referer ? $this->redirect($referer) : $this->redirectToRoute('home');
    }

    #[Route('/comment/rating/{id}/{score}', name: 'comment_rating')]
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function ratingComment(Request $request, Comment $comment, int $score, EntityManagerInterface $em, VoteRepository $voteRepository, CommentRepository $commentRepository)
    {

        $user = $this->getUser();

        if ($user !== $comment->getAuthor()) {
            $vote = $voteRepository->findOneBy(['author' => $user, 'comment' => $comment]);

            if ($vote) {

                if (($vote->getIsLiked() && $score > 0) || (!$vote->getIsLiked() && $score < 0)) {
                    $em->remove($vote);
                    $comment->setRating($comment->getRating() + ($score > 0 ? -1 : 1));
                } else {
                    $vote->setIsLiked(!$vote->getIsLiked());
                    $comment->setRating($comment->getRating() + ($score > 0 ? 2 : -2));
                }
            } else {
                $vote = new Vote();
                $vote->setAuthor($user);
                $vote->setComment($comment);
                $vote->setIsLiked($score > 0 ? true : false);
                $comment->setRating($comment->getRating() + $score);
                $em->persist($vote);
            }

            $em->flush();
        }


        $referer = $request->server->get('HTTP_REFERER');
        return $referer ? $this->redirect($referer) : $this->redirectToRoute('home');
    }






    #[Route('/question/user/list', name: 'app_my_questions')]
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function userQuestion(QuestionRepository $questionRepository): Response
    {
        $user = $this->getUser();
        if ($user) {
            $id = $user->getId();
            $userQuestion = $questionRepository->findBy(['author' => $id]);
        } else {
            return $this->redirectToRoute('app_login');
        }
        return $this->render('question/user_question.html.twig', [
            'question' => $userQuestion,
        ]);
    }

    #[Route('question/response/user/list', name: 'app_my_responses')]
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function userResponse(CommentRepository $commentRepository): Response
    {
        $user = $this->getUser();
        if ($user) {
            $id = $user->getId();
            $userResponse = $commentRepository->findBy(['author' => $id]);
        } else {
            return $this->redirectToRoute('app_login');
        }
        return $this->render('question/user_response.html.twig', [
            'response' => $userResponse,
        ]);
    }
}
