<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Entity\ResetPassword;
use App\Form\RequestResetPasswordType;
use App\Form\ResetPasswordType;
use App\Repository\ResetPasswordRepository;
use App\Repository\UserRepository;
use App\Service\Uploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\RateLimiter\RateLimiterFactory;
use Symfony\Component\Security\Http\Authenticator\FormLoginAuthenticator;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class SecurityController extends AbstractController
{

    public function __construct(private FormLoginAuthenticator $authenticator)
    {
    }

    #[Route('/signup', name: 'app_signup')]
    public function signup(Uploader $uploader, UserAuthenticatorInterface $userAuthenticatorInterface, Request $request, EntityManagerInterface $em, UserPasswordHasherInterface $userPasswordHasherInterface, MailerInterface $mailer): Response
    {

        $user = new User();
        $userForm = $this->createForm(UserType::class, $user);
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $picture = $userForm->get('pictureFile')->getData();
            $user->setPicture($uploader->uploadProfilImage($picture));
            //$user->setPassword($userPasswordHasherInterface->hashPassword($user, $userForm->get('password')->getData()));
            //$user->setCreatedAt(new \DateTimeImmutable());
            $em->persist($user);
            $em->flush();
            //$this->addFlash('success', 'Votre compte a bien été créé');

            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addSuccess('Votre compte a bien été créé.');


            //dd('Sending welcome email to ' . $user->getEmail());

            // Pour envoyer un mail de confirmation

            $email = (new TemplatedEmail())
                ->to($user->getEmail())
                ->subject('Welcome to WonderApp')
                ->htmlTemplate('@email_templates/welcome.html.twig')
                ->context([
                    'username' => $user->getFullName(),
                ]);

            $mailer->send($email);

            //return $this->redirectToRoute('app_login');

            // Pour connecter l'utilisateur directement après son inscription
            return $userAuthenticatorInterface->authenticateUser(
                $user,
                $this->authenticator,
                $request
            );
        }

        return $this->render('security/signup.html.twig', [
            'form' => $userForm->createView(),
        ]);
    }

    #[Route('/login', name: 'app_login')]
    public function login(RateLimiterFactory $loginAttemptsLimiter, AuthenticationUtils $authenticationUtils, Request $request): Response
    {

        if ($this->getUser()) {
            return $this->redirectToRoute('app_home');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $username = $authenticationUtils->getLastUsername();

        $loginForm = $this->createForm(UserType::class);
        $loginForm->handleRequest($request);

        if ($loginForm->isSubmitted() && $loginForm->isValid()) {
            $limiter = $loginAttemptsLimiter->create($request->getClientIp());

            if (!$limiter->consume(1)->isAccepted()) {
                //$this->addFlash('error', 'Vous avez atteint la limite. Veuillez réessayer dans 1 heure');

                notyf()
                    ->position('x', 'center')
                    ->position('y', 'top')
                    ->duration(5000)
                    ->dismissible(true)
                    ->addError('Vous avez atteint la limite. Veuillez réessayer dans 1 heure');
                return $this->redirectToRoute('app_home');
            }
        }


        return $this->render('security/login.html.twig', [
            'error' => $error,
            'username' => $username,
        ]);
    }


    #[Route('/logout', name: 'app_logout')]
    public function logout()
    {
    }


    #[Route('/forgot-password/{token}', name: 'app_reset_password')]
    public function resetPassword(RateLimiterFactory $passwordRecoveryLimiter, UserPasswordHasherInterface $userPasswordHasher, string $token, Request $request, ResetPasswordRepository $resetPasswordRepository, EntityManagerInterface $em): Response
    {

        $limiter = $passwordRecoveryLimiter->create($request->getClientIp());
        //consume() permet de consommer une unité de la limite. ici on décrémente de 1 le nombre de tentative de connexion qui est de 3 dans rate_limiter.yaml

        if (!$limiter->consume(1)->isAccepted()) {
            //$this->addFlash('error', 'Vous avez atteint la limite.Veuillez réessayer dans 1 heure');
            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addError('Vous avez atteint la limite.Veuillez réessayer dans 1 heure');
            return $this->redirectToRoute('app_login');
        }

        $resetPassword = $resetPasswordRepository->findOneBy(['token' => sha1($token)]);

        if (!$resetPassword || $resetPassword->getExpiredAt() < new \DateTimeImmutable()) {
            if ($resetPassword) {
                $em->remove($resetPassword);
                $em->flush();
            }
            //$this->addFlash('warning', 'Le lien de réinitialisation a expiré.Veuillez refaire une nouvelle demande');
            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addWarning('Le lien de réinitialisation a expiré.Veuillez refaire une nouvelle demande');
            return $this->redirectToRoute('app_login');
        }

        $resetPasswordForm = $this->createForm(RequestResetPasswordType::class);
        $resetPasswordForm->handleRequest($request);

        if ($resetPasswordForm->isSubmitted() && $resetPasswordForm->isValid()) {

            $user = $resetPassword->getUser();

            $user->setPassword($userPasswordHasher->hashPassword($user, $resetPasswordForm->get('password')->getData()));

            $em->remove($resetPassword);
            $em->flush();

            //$this->addFlash('success', 'Votre mot de passe a bien été modifié');
            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addSuccess('Votre mot de passe a bien été modifié');
            return $this->redirectToRoute('app_login');
        }


        return $this->render('security/reset_password.html.twig', [
            'form' => $resetPasswordForm->createView(),
        ]);
    }


    #[Route('/forgot-password', name: 'app_forgot_password')]
    public function forgotPassword(Request $request, UserRepository $userRepository, ResetPasswordRepository $resetPasswordRepository, EntityManagerInterface $em, MailerInterface $mailer, RateLimiterFactory $passwordRecoveryLimiter): Response
    {
        $currentUser = $this->getUser();
        if ($currentUser) {
            //$this->addFlash('info', 'Vous êtes déjà connecté');
            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addInfo('Vous êtes déjà connecté');
            return $this->redirectToRoute('app_home');
        }

        $limiter = $passwordRecoveryLimiter->create($request->getClientIp());
        //consume() permet de consommer une unité de la limite. ici on décrémente de 1 le nombre de tentative de connexion qui est de 3 dans rate_limiter.yaml

        if (!$limiter->consume(1)->isAccepted()) {
            //$this->addFlash('error', 'Vous avez atteint la limite de demande de réinitialisation de mot de passe.Veuillez réessayer dans 1 heure');
            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addError('Vous avez atteint la limite de demande de réinitialisation de mot de passe.Veuillez réessayer dans 1 heure');
            return $this->redirectToRoute('app_login');
        }


        $emailForm = $this->createForm(ResetPasswordType::class);
        $emailForm->handleRequest($request);

        if ($emailForm->isSubmitted() && $emailForm->isValid()) {

            $emailValue = $emailForm->get('email')->getData();

            $user = $userRepository->findOneBy(['email' => $emailValue]);

            if ($user) {

                $oldResetPassword =  $resetPasswordRepository->findOneBy(['user' => $user]);

                if ($oldResetPassword) {
                    $em->remove($oldResetPassword);
                    $em->flush();
                }

                $resetPassword = new ResetPassword();
                $resetPassword->setUser($user);
                $resetPassword->setExpiredAt(new \DateTimeImmutable('+1 hour'));
                $token = substr(str_replace(['+', '/', '='], '', base64_encode(random_bytes(64))), 0, 64);
                $resetPassword->setToken(sha1($token));
                $resetPassword->setCreatedAt(new \DateTimeImmutable());
                $em->persist($resetPassword);
                $em->flush();

                $email = (new TemplatedEmail())
                    ->to($emailValue)
                    ->subject('Demande de réinitialisation de votre mot de passe')
                    ->htmlTemplate('@email_templates/reset_password_email.html.twig')
                    ->context([
                        'username' => $user->getFullName(),
                        'token' => $token,
                        'tokenLifetime' => $resetPassword->getExpiredAt()->format('H:i'),
                    ]);

                $mailer->send($email);
            }
            //$this->addFlash('success', 'Un email vous a été envoyé pour réinitialiser votre mot de passe');
            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addSuccess('Un email vous a été envoyé pour réinitialiser votre mot de passe');
            return $this->redirectToRoute('app_login');
        }


        return $this->render('security/forgot_password.html.twig', [
            'form' => $emailForm->createView(),
        ]);
    }
}
