<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Service\Uploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class UserController extends AbstractController
{


    #[Route('/user', name: 'app_current_user_profile')]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function currentUserProfile(Uploader $uploader, Request $request, EntityManagerInterface $em, UserPasswordHasherInterface $userPassword): Response
    {
        $user = $this->getUser();

        $userForm = $this->createForm(UserType::class, $user);


        $userForm->remove('password'); // Suppression du champ password du formulaire

        // Ajout du champ newPassword au formulaire pour permettre la modification du mot de passe
        $userForm->add('newPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'Les mots de passe doivent correspondre.',
            'required' => false,
            'first_options' => ['label' => 'Nouveau mot de passe', 'required' => false],
            'second_options' => ['label' => 'Confirmer le nouveau mot de passe', 'required' => false],
        ]);

        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {

            $newPassword = $user->getNewPassword();

            if ($newPassword) {
                $hash = $userPassword->hashPassword($user, $newPassword);
                $user->setPassword($hash);
            }

            $picture = $userForm->get('pictureFile')->getData();
            if ($picture) {
                $user->setPicture($uploader->uploadProfilImage($picture, $user->getPicture()));
            }

            $em->flush();

            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addSuccess('Votre profil a bien été mis à jour.');


            /* $this->addFlash('info', 'Votre profil a bien été mis à jour.'); */
        }

        return $this->render('user/index.html.twig', [
            'form' => $userForm->createView(),
        ]);
    }

    #[Route('/user/{id}/edit', name: 'app_user_edit')]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function editCurrentUserProfile(User $user, $id, Uploader $uploader, Request $request, EntityManagerInterface $em, UserPasswordHasherInterface $userPassword): Response
    {

        $user = $this->getUser();

        $userForm = $this->createForm(UserType::class, $user);


        $userForm->remove('password'); // Suppression du champ password du formulaire

        // Ajout du champ newPassword au formulaire pour permettre la modification du mot de passe
        /*  $userForm->add('newPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'Les mots de passe doivent correspondre.',
            'required' => false,
            'first_options' => ['label' => 'Nouveau mot de passe', 'required' => false],
            'second_options' => ['label' => 'Confirmer le nouveau mot de passe', 'required' => false],
        ]);
 */
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {

            /*  $newPassword = $user->getNewPassword();

            if ($newPassword) {
                $hash = $userPassword->hashPassword($user, $newPassword);
                $user->setPassword($hash);
            } */

            $picture = $userForm->get('pictureFile')->getData();
            if ($picture) {
                $user->setPicture($uploader->uploadProfilImage($picture, $user->getPicture()));
            }

            $em->flush();
            /*  $this->addFlash('info', 'Votre profil a bien été mis à jour.'); */
            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addSuccess('Votre profil a bien été mis à jour.');
        }

        return $this->render('user/edit.html.twig', [
            'form' => $userForm->createView(),
        ]);
    }


    #[Route('/user/{id}/change-password', name: 'app_user_change_password')]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function changeCurrentUserProfile(User $user, $id, Uploader $uploader, Request $request, EntityManagerInterface $em, UserPasswordHasherInterface $userPassword): Response
    {

        $user = $this->getUser();

        $userForm = $this->createForm(UserType::class, $user);


        $userForm->remove('gender');
        $userForm->remove('email');
        $userForm->remove('firstName');
        $userForm->remove('lastName');
        $userForm->remove('pictureFile');
        $userForm->remove('password');


        // Ajout du champ newPassword au formulaire pour permettre la modification du mot de passe
        $userForm->add('newPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'Les mots de passe doivent correspondre.',
            'required' => false,
            'first_options' => ['label' => 'Nouveau mot de passe', 'required' => false],
            'second_options' => ['label' => 'Confirmer le nouveau mot de passe', 'required' => false],
        ]);

        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {

            $newPassword = $user->getNewPassword();

            if ($newPassword) {
                $hash = $userPassword->hashPassword($user, $newPassword);
                $user->setPassword($hash);
            }


            $em->flush();
            /* $this->addFlash('info', 'Votre mot de passe a bien été mis à jour.'); */


            notyf()
                ->position('x', 'center')
                ->position('y', 'top')
                ->duration(5000)
                ->dismissible(true)
                ->addSuccess('Votre mot de passe a bien été mis à jour.');

            return $this->redirectToRoute('app_current_user_profile');
        }

        return $this->render('user/reset_password.html.twig', [
            'form' => $userForm->createView(),
        ]);
    }





    #[Route('/user/{id}', name: 'app_user_profile')]
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function userProfile(User $user): Response
    {
        $currentUser = $this->getUser();

        if ($currentUser === $user) {
            return $this->redirectToRoute('app_current_user_profile');
        }

        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }
}
