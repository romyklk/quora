<?php

namespace App\Form;

use App\Entity\User;
use PharIo\Manifest\Email;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // gestion de la modification de la photo de profil
        $user = $builder->getData();

        $builder
            ->add('gender',ChoiceType::class, [
                'choices' => [
                ' Monsieur ' => 'men',
                ' Madame ' => 'women',
                ' Autre ' => 'lego',
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => 'Civilité',
                'required' => true,
            ])

            ->add('email',EmailType::class, [
                'label' => 'E-mail',
                'required' => true,
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne correspondent pas',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmer le mot de passe'],
            ])
            ->add('firstName',TextType::class, [
                'label' => 'Prénom',
                'required' => true,
            ])
            ->add('lastName',TextType::class, [
                'label' => 'Nom',
                'required' => true,
            ])
            ->add('pictureFile',FileType::class, [
                'label' => 'Photo de profil',
                'mapped' => false,
                'required' => $user?->getPicture() ? false : true,
                'constraints'=>[
                    new Image([
                        'maxSize' => '3M',
                        'mimeTypesMessage' => 'Veuillez sélectionner une image au format jpg, jpeg, png',
                        'mimeTypes' => [
                            'image/jpg',
                            'image/jpeg',
                            'image/png',
                            'image/webp',
                        ],
                        'maxSizeMessage' => 'Votre image fait {{ size }} {{ suffix }}. La taille maximale autorisée est de {{ limit }} {{ suffix }}.',
                    ])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
