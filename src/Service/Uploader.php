<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

// Le ParameterBag est un service qui permet de récupérer les paramètres de configuration de notre application. C'est un service qui est injecté automatiquement dans tous les services et les contrôleurs.
class Uploader
{

    public function __construct(private Filesystem $filesystem,private $profilePictureDirectory,private $profilePublicDirectoryPath)
    {
    }

    public function uploadProfilImage(UploadedFile $picture, string $oldPicturePath=null)
    {
        $ext = $picture->guessExtension();
        $filename = uniqid() . '' . bin2hex(random_bytes(16)) . '' . time() . '.' . $ext;
        $picture->move($this->profilePictureDirectory, $filename);

        if($oldPicturePath){
            $filesystem = new Filesystem();
            $filesystem->remove($this->profilePictureDirectory.'/'.pathinfo($oldPicturePath,PATHINFO_BASENAME));
        }
        $fileUrl = $this->profilePublicDirectoryPath . '/' . $filename;
        return $fileUrl;
    }
}
